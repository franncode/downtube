import { VideoThumbnail } from '../../components/videoThumbnail/videoThumbnail'
import { TitleSection } from '../../components/titleSection/titleSection'
import { TextSection } from '../../components/textSection/textSection'
import { DescriptionItem } from '../../components/descriptionItem/descriptionItem'
import styles from './[videoId].scss'
import { useEffect } from 'react'

const Video = ({ videoId }) => {
	// useEffect(() => {
	// 	const getVideoInfo = async videoId => {
	// 		const videoInfoResponse = fetch(
	// 			`http://youtube.com/get_video_info?video_id=${videoId}`
	// 		)
	// 		const videoInfo = await videoInfoResponse.json()
	// 		console.log('videoInfo', videoInfo)
	// 	}
	// 	getVideoInfo()
	// }, [])

	return (
		<div className={styles.video}>
			<VideoThumbnail
			// videoId={videoId}
			/>
			<TitleSection text='Information' margin={[24, 0, 12, 24]} />
			<TextSection
				text='This is the title of the video'
				margin={[0, 0, 0, 24]}
			/>
			<div className={styles.description}>
				<DescriptionItem
					icon='/icons/eye.svg'
					title='422'
					subtitle='Visualizations'
					margin={[0, 30, 0, 0]}
				/>
				<DescriptionItem
					icon='/icons/clock.svg'
					title='10 minutos'
					subtitle='Duration'
				/>
			</div>
			<TitleSection text='Download options' margin={[0, 0, 0, 24]} />
		</div>
	)
}

Video.getInitialProps = async ({ query }) => {
	const { videoId } = await query
	return { videoId }
}

export default Video
