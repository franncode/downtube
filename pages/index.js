import { useState, useEffect } from 'react'
import { SearchBar } from '../components/searchBar/searchBar'
import { ActionButton } from '../components/actionButton/actionButton'
import { Logo } from '../components/logo/logo'
import styles from './index.scss'

export default function Home() {
	const [searchText, setSearchText] = useState('')

	useEffect(() => {
		const isValidLink = () => {
			const validLink = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/
		}
	}, [searchText])

	const handlePaste = async () => {
		let copiedText = await navigator.clipboard.readText()
		setSearchText('copiedText')
	}

	return (
		<div className={styles.home}>
			<Logo margin={[120, 0, 0, 0]} />
			<p style={{ margin: '12px 0px' }}>
				your favorite music and videos now offline
			</p>
			<div>
				<SearchBar
					value={searchText}
					onChange={({ value }) => {
						setSearchText(value)
					}}
					placeholder='insert youtube link'
				/>
				<ActionButton
					text='paste'
					icon='/icons/paste.svg'
					onClick={handlePaste}
				/>
			</div>
		</div>
	)
}
