import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class customDocument extends Document {
	render() {
		return (
			<Html lang='en'>
				<Head>
					<title>Downtube</title>
					<link
						rel='shortcut icon'
						type='image/png'
						href='/imgs/appIcon48.png'
					/>
					<link rel='manifest' href='/manifest.json' />
					<meta charSet='utf-8' />
					<meta name='copyright' content='Downtube' />
					<meta
						name='author'
						content='Francisco Rodriguez, francisco@vectores.io'
					/>
					<meta
						name='description'
						content='Download video from youtube in mp3 or mp4 format'
					/>

					<meta property='og:title' content='Downtube' />
					<meta property='og:url' content='https://www.downtube.now.sh' />
					<meta property='og:type' content='website' />
					<meta property='og:site_name' content='Downtube' />
					<meta property='og:description' content='Downtube' />
					<meta
						name='viewport'
						content='width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0'
					/>
					<meta name='theme-color' content='#d32f2f' />
					<link rel='apple-touch-icon' href='/imgs/appIcon192.png' />
					<meta name='apple-mobile-web-app-title' content='Downtube' />
					<meta name='apple-mobile-web-app-capable' content='yes' />
					<meta name='mobile-web-app-capable' content='yes' />
					<meta
						name='apple-mobile-web-app-status-bar-style'
						content='black-translucent'
					/>
					<meta name='apple-mobile-web-app-capable' content='yes' />
					<link
						href='/launch/apple_splash_750.png'
						sizes='750x1334'
						rel='apple-touch-startup-image'
					/>
					<link
						href='/launch/apple_splash_1125.png'
						sizes='1125x2436'
						rel='apple-touch-startup-image'
					/>
					<link
						href='/launch/apple_splash_1242.png'
						sizes='1242x2208'
						rel='apple-touch-startup-image'
					/>
					<link
						href='/launch/apple_splash_640.png'
						sizes='640x1136'
						rel='apple-touch-startup-image'
					/>
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		)
	}
}
