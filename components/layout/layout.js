import styles from './layout.scss'

export const Layout = ({ children }) => {
	return <section className={styles.layout}>{children}</section>
}
