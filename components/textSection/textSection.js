import { marginCalc } from '../../utils/marginCalc'
import styles from './textSection.scss'

export const TextSection = ({ text, margin }) => {
	return (
		<p className={styles.textSection} style={marginCalc(margin)}>
			{text}
		</p>
	)
}
