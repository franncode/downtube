import styles from './videoThumbnail.scss'

export const VideoThumbnail = ({ videoId = 'fAgvmh1jaCE' }) => {
	return (
		<div className={styles.videoThumbnails}>
			{/* <img
				src='https://img.youtube.com/vi/fAgvmh1jaCE/0.jpg'
				alt='Video thumbnail'
			/> */}
			<img
				src={`https://img.youtube.com/vi/${videoId}/0.jpg`}
				alt='Video thumbnail'
			/>
		</div>
	)
}
