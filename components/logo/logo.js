import { marginCalc } from '../../utils/marginCalc'
import styles from './logo.scss'

export const Logo = ({ margin }) => (
	<h3 className={styles.logo} style={marginCalc(margin)}>
		down<span>tube</span>
	</h3>
)
