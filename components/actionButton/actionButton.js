import { marginCalc } from '../../utils/marginCalc'
import styles from './actionButton.scss'

export const ActionButton = ({ icon, text, onClick, margin }) => {
	return (
		<button
			className={styles.actionButton}
			style={marginCalc(margin)}
			onClick={e => onClick(e)}
		>
			{icon && <img src={icon} alt='icon button' />}
			{text}
		</button>
	)
}
