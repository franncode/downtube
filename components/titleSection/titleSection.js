import { marginCalc } from '../../utils/marginCalc'
import styles from './titleSection.scss'

export const TitleSection = ({ text, margin }) => {
	return (
		<h3 className={styles.titleSection} style={marginCalc(margin)}>
			{text}
		</h3>
	)
}
