import { marginCalc } from '../../utils/marginCalc'
import styles from './descriptionItem.scss'

export const DescriptionItem = ({ icon, title, subtitle, margin }) => {
	return (
		<div className={styles.descriptionItem} style={marginCalc(margin)}>
			<img src={icon} alt='icono' />
			<div>
				<p>{title}</p>
				<p>{subtitle}</p>
			</div>
		</div>
	)
}
